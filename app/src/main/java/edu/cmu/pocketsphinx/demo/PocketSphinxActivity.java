/* ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ALPHA CEPHEI INC. ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL CARNEGIE MELLON UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 */

package edu.cmu.pocketsphinx.demo;

import android.Manifest;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.icu.util.Calendar;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;

import static android.widget.Toast.makeText;

public class PocketSphinxActivity extends Activity implements
        edu.cmu.pocketsphinx.RecognitionListener {
    private TextToSpeech myTTS;
    private android.speech.SpeechRecognizer sp;
    private SpeechRecognizer recognizer;

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String FORECAST_SEARCH = "forecast";
    private static final String DIGITS_SEARCH = "digits";
    private static final String PHONE_SEARCH = "phones";
    private static final String MENU_SEARCH = "menu";
    private static final String encoding = "UTF-8";
    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "ascolta";
    public int s = 0;
    public boolean calcoli = false ;
    /* Used to handle permission request */
    public Boolean gioco = false;
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;


    private HashMap<String, Integer> captions;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        // Prepare the data for UI
        captions = new HashMap<>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        captions.put(MENU_SEARCH, R.string.menu_caption);
        captions.put(DIGITS_SEARCH, R.string.digits_caption);
        captions.put(PHONE_SEARCH, R.string.phone_caption);
        captions.put(FORECAST_SEARCH, R.string.forecast_caption);
        setContentView(R.layout.main);
        ((TextView) findViewById(R.id.caption_text))
                .setText("Avviando l'applicazione");

        // Check if user has given permission to record audio
        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        // Recognizer initialization is a time-consuming and it involves IO,
        // so we execute it in async task
        new SetupTask(this).execute();
        initializeTextToSpeech();
        initializesp();
    }


    private void initializesp() {
        if (sp.isRecognitionAvailable(this)) {
            sp = android.speech.SpeechRecognizer.createSpeechRecognizer(this);
            sp.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle bundle) {
                    s = 0;
                    new CountDownTimer(10000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            if (s == 1) {
                                cancel();
                            }
                        }

                        public void onFinish() {
                            speak1("non ho capito");
                            switchSearch(KWS_SEARCH);
                            return;
                        }

                    }.start();
                }

                @Override
                public void onBeginningOfSpeech() {

                }

                @Override
                public void onRmsChanged(float v) {

                }

                @Override
                public void onBufferReceived(byte[] bytes) {

                }

                @Override
                public void onEndOfSpeech() {

                }

                @Override
                public void onError(int i) {

                }

                @Override
                public void onResults(Bundle bundle) {
                    s = 1;
                    List<String> r;
                    r = bundle.getStringArrayList(sp.RESULTS_RECOGNITION);
                    risultato(r.get(0));
                }

                @Override
                public void onPartialResults(Bundle bundle) {

                }

                @Override
                public void onEvent(int i, Bundle bundle) {

                }
            });


        }
    }

    private void risultato(String comando) {
        boolean s = false;
        try {
            comando = comando.toLowerCase();

            if (comando.indexOf("nome") != -1) {

                speak1("Il mio nome è Uan");


            } else if (comando.indexOf("ore") != -1) {
                Date ora = new Date();
                String tempo = DateUtils.formatDateTime(this, ora.getTime(), DateUtils.FORMAT_SHOW_TIME);
                speak1("Sono le" + tempo);
                s = true;

            } else if (comando.indexOf("giorno") != -1) {
                String Mese = "";
                String Anno = "";
                Calendar now = Calendar.getInstance();
                if (now.get(Calendar.MONTH) + 1 == 1) {
                    Mese = "Gennaio";
                } else if (now.get(Calendar.MONTH) + 1 == 2) {
                    Mese = "Febraio";
                } else if (now.get(Calendar.MONTH) + 1 == 3) {
                    Mese = "Marzo";
                } else if (now.get(Calendar.MONTH) + 1 == 4) {
                    Mese = "Aprile";
                } else if (now.get(Calendar.MONTH) + 1 == 5) {
                    Mese = "Maggio";
                } else if (now.get(Calendar.MONTH) + 1 == 6) {
                    Mese = "Giugno";
                } else if (now.get(Calendar.MONTH) + 1 == 7) {
                    Mese = "Luglio";
                } else if (now.get(Calendar.MONTH) + 1 == 8) {
                    Mese = "Agosto";
                } else if (now.get(Calendar.MONTH) + 1 == 9) {
                    Mese = "Settembre";
                } else if (now.get(Calendar.MONTH) + 1 == 10) {
                    Mese = "Ottobre";
                } else if (now.get(Calendar.MONTH) + 1 == 11) {
                    Mese = "Novembre";
                } else if (now.get(Calendar.MONTH) + 1 == 12) {
                    Mese = "Dicembre";
                }

                if (now.get(Calendar.YEAR) == 2019) {
                    Anno = "duemiladiciannove";
                } else if (now.get(Calendar.YEAR) == 2020) {
                    Anno = "duemilaventi";
                }
                speak1(String.valueOf(now.get(Calendar.DAY_OF_MONTH)) + String.valueOf(Mese) + Anno);
                s = true;


            } else if (comando.indexOf("waze") != -1) {
                int cm = 0;
                String url = null;
                String[] dizionario = {"portami", "cerca", "waze", "trova"};
                String[] parole = comando.split(" ");
                String p = "";
                for (int i = 0; i < parole.length; i++) {
                    for (int j = 0; j < dizionario.length; j++) {
                        if (parole[i].equals(dizionario[j])) {
                            cm = i;
                        }
                    }
                }
                for (int i = cm + 1; i < parole.length; i++) {
                    p = p + parole[i] + " ";
                }


                url = "waze://?q=" + p;


                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                s = true;
            } else if (comando.indexOf("youtube") != -1) {
                int cm = 0;

                String[] dizionario = {"cerca", "trova", "youtube"};
                String[] parole = comando.split(" ");
                String p = "";
                for (int i = 0; i < parole.length; i++) {
                    for (int j = 0; j < dizionario.length; j++) {
                        if (parole[i].equals(dizionario[j])) {
                            cm = i;
                        }
                    }
                }
                for (int i = cm + 1; i < parole.length; i++) {
                    p = p + parole[i] + " ";
                }
                String url = "https://www.youtube.com/results?search_query=" + p;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                s = true;

            } else if (comando.indexOf("spotify") != -1) {
                int cm = 0;
                String[] dizionario = {"cerca", "trova", "spotify"};
                String[] parole = comando.split(" ");
                String p = "";
                for (int i = 0; i < parole.length; i++) {
                    for (int j = 0; j < dizionario.length; j++) {
                        if (parole[i].equals(dizionario[j])) {
                            cm = i;
                        }
                    }
                }
                for (int i = cm + 1; i < parole.length; i++) {
                    p = p + parole[i] + " ";
                }

                //intent.setAction(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH);



                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setAction(MediaStore.INTENT_ACTION_MEDIA_PLAY_FROM_SEARCH);
                intent.setComponent(new ComponentName(
                        "com.spotify.music",
                        "com.spotify.music.MainActivity"));
                intent.putExtra(SearchManager.QUERY, "p");
                startActivity(intent);
                s = true;


            } else if (comando.indexOf("google") != -1) {
                int cm = 0;

                String[] dizionario = {"cerca", "trova", "google"};
                String[] parole = comando.split(" ");
                String p = "";
                for (int i = 0; i < parole.length; i++) {
                    for (int j = 0; j < dizionario.length; j++) {
                        if (parole[i].equals(dizionario[j])) {
                            cm = i;
                        }
                    }
                }
                for (int i = cm + 1; i < parole.length; i++) {
                    p = p + parole[i] + " ";
                }

                makeText(getApplicationContext(), comando, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/search?q=" + p));
                startActivity(intent);
                s = true;


            } else if (comando.indexOf("wiki") != -1) {

                int cm=0;

                String[] dizionario = {"cerca","trova","wikipedia"};
                String[] parole = comando.split(" ");
                String p="";
                for(int i=0;i<parole.length;i++)
                {
                    for(int j=0;j<dizionario.length;j++)
                    {
                        if(parole[i].equals(dizionario[j]))
                        {
                            cm=i;
                        }
                    }
                }
                for(int i=cm+1;i<parole.length;i++)
                {
                    p=p+parole[i]+" ";
                }
                makeText(getApplicationContext(), p, Toast.LENGTH_SHORT).show();
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                params.put("s", p);
                client.post("http://pxgamers.altervista.org/One/wikipedia.php", params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {
                                    InputStream is = null;
                                    BufferedReader bfReader = null;
                                    is = new ByteArrayInputStream(responseBody);
                                    bfReader = new BufferedReader(new InputStreamReader(is));
                                    String temp = null;
                                    String b = "Wikipedia dice ";
                                    while ((temp = bfReader.readLine()) != null) {
                                        b = b + temp;
                                    }
                                    speak1(b);


                                } catch (Exception e) {
                                    speak1("non hai internet");
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                            }

                            @Override
                            public void onRetry(int retryNo) {
                                super.onRetry(retryNo);
                            }

                            @Override
                            public void onStart() {
                                super.onStart();
                            }
                        }
                );

                s=true;

            } else if (comando.indexOf("canzone") != -1) {
                if (comando.indexOf("successiva") != -1) {
                    Intent playSpotify = new Intent("com.spotify.mobile.android.ui.widget.NEXT");
                    playSpotify.setPackage("com.spotify.music");
                    sendBroadcast(playSpotify);
                    s=true;
                } else if (comando.indexOf("precedente") != -1) {
                    Intent playSpotify = new Intent("com.spotify.mobile.android.ui.widget.PREVIOUS");
                    playSpotify.setPackage("com.spotify.music");
                    sendBroadcast(playSpotify);
                    s=true;

                }
            } else if (comando.indexOf("accendi") != -1) {
                if (comando.indexOf("torcia") != -1) {

                    Camera camera = Camera.open();
                    Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(p);
                    camera.startPreview();
                    s=true;

                }
            } else if (comando.indexOf("spegni") != -1) {
                if (comando.indexOf("torcia") != -1) {
                    Camera camera = Camera.open();
                    Camera.Parameters p = camera.getParameters();
                    p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(p);
                    camera.stopPreview();
                    s=true;

                }
            } else if (comando.indexOf("tempo") != -1) {
                if (comando.indexOf("oggi") != -1) {
                    int i = comando.indexOf("oggi");
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    params.put("n", comando.substring(i + 7));
                    client.post("http://johnhack12.altervista.org/Tempo.php", params, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                    try {

                                        InputStream is = null;
                                        BufferedReader bfReader = null;
                                        is = new ByteArrayInputStream(responseBody);
                                        bfReader = new BufferedReader(new InputStreamReader(is));
                                        String temp = null;
                                        String b = "";
                                        while ((temp = bfReader.readLine()) != null) {
                                            b = b + temp;
                                        }
                                        speak1(b);


                                    } catch (Exception e) {
                                        speak1("non hai internet");
                                    }

                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                                }

                                @Override
                                public void onRetry(int retryNo) {
                                    super.onRetry(retryNo);
                                }

                                @Override
                                public void onStart() {
                                    super.onStart();
                                }
                            }
                    );
                }
                s=true;
            }else if (comando.indexOf("testa o croce")!=-1)
            {
                final MediaPlayer mp = MediaPlayer.create(this,R.raw.s);

                if(mp.isPlaying() == false);
                mp.start();
                    mp
                            .setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                public void onCompletion(MediaPlayer mp) {
                                    mp.release();
                                    Random rand = new Random();
                                    int d=rand.nextInt(2);
                                    if(d==0)
                                    {
                                        speak1("è uscita croce");
                                    }else {
                                        speak1("è uscita testa");
                                    }

                                };
                            });
            }else if (comando.indexOf("creato")!=-1||comando.indexOf("progettato")!=-1||comando.indexOf("ideato")!=-1)
            {
              speak1("i miei creatori sono silvio fosso e christian mileto.....due programmatori falliti");
            }else if (comando.indexOf("calcolatrice")!=-1) {
            calcoli=true;
            speak1("certo, dimmi");
            while(myTTS.isSpeaking())
            {

            }
            parla();
            return;
            }else if (calcoli==true)
            {
                //makeText(getApplicationContext(), comando, Toast.LENGTH_SHORT).show();

                calcoli=false;
                int cm=0;
                boolean ver = false;
                String segno =null;
                int somma=0;
                String[] dizionario = {"fai"};
                String[] parole = comando.split(" ");
                String p="";
                String[] perc = comando.split("%");
               // makeText(getApplicationContext(), perc[1], Toast.LENGTH_SHORT).show();
                for(int i=0;i<parole.length;i++)
                {
                    for(int j=0;j<dizionario.length;j++)
                    {
                        if(parole[i].equals(dizionario[j]))
                        {
                            ver = true;
                        }
                    }
                }

                if(ver==true)
                {
                     segno = parole[2];
                     if(segno.equals("+"))
                    {
                        somma = Integer.parseInt(parole[1]) + Integer.parseInt(parole[3]);
                        speak1(String.valueOf(somma));
                    } else if(segno.equals("meno"))
                    {
                        int sottrazione =  Integer.parseInt(parole[1]) - Integer.parseInt(parole[3]);
                        speak1(String.valueOf(sottrazione));
                    } else if(segno.equals("x"))
                    {
                        int moltiplicazione =  Integer.parseInt(parole[1]) * Integer.parseInt(parole[3]);
                        speak1(String.valueOf(moltiplicazione));
                    } else if(segno.equals("diviso"))
                    {
                        int diviso =  Integer.parseInt(parole[1]) / Integer.parseInt(parole[3]);
                        speak1(String.valueOf(diviso));
                    }else if(perc[1]!=null)
                {


                    int numver = Integer.parseInt(perc[0]);
                    int n =0;
                    //  speak1(String.valueOf(numver));
                    int n2 = numver;
                    //  makeText(getApplicationContext(), numver, Toast.LENGTH_SHORT).show();
                    int n1 = Integer.parseInt(parole[2]);
                    n= (n2*n1)/100;
                    speak1(String.valueOf(n));

                }
                }else {
                    segno= parole[1];
                   // makeText(getApplicationContext(), segno, Toast.LENGTH_SHORT).show();
                     if(segno.equals("+"))
                    {

                        somma = Integer.parseInt(parole[0]) + Integer.parseInt(parole[2]);
                        speak1(String.valueOf(somma));
                    }else if(segno.equals("meno"))
                    {
                        int sottrazione =  Integer.parseInt(parole[0]) - Integer.parseInt(parole[2]);
                        speak1(String.valueOf(sottrazione));
                    } else if(segno.equals("*"))
                    {
                        int moltiplicazione =  Integer.parseInt(parole[0]) * Integer.parseInt(parole[2]);
                        speak1(String.valueOf(moltiplicazione));

                    } else if(segno.equals("diviso"))
                    {
                        int diviso =  Integer.parseInt(parole[0]) / Integer.parseInt(parole[2]);
                        speak1(String.valueOf(diviso));

                    }else if(perc[1]!=null)
                    {

                        int numver = Integer.parseInt(perc[0]);
                        int n =0;
                        //speak1(String.valueOf(numver));
                        int n2 = numver;

                        int n1 = Integer.parseInt(parole[2]);
                        n= (n2*n1)/100;
                        speak1(String.valueOf(n));

                    }

                }


            }else if (comando.indexOf("chiama")!=-1){
                int cm=0;

                String[] dizionario = {"chiama"};
                String[] parole = comando.split(" ");
                String p="";
                for(int i=0;i<parole.length;i++)
                {
                    for(int j=0;j<dizionario.length;j++)
                    {
                        if(parole[i].equals(dizionario[j]))
                        {
                            cm=i;
                        }
                    }
                }
                for(int i=cm+1;i<parole.length;i++)
                {
                    p=p+parole[i];
                }
                makeText(getApplicationContext(), p, Toast.LENGTH_SHORT).show();
                if(p.equals("mamma"))
                {

                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3202780264"));
                    startActivity(intent);
                }else  if(p.equals("cristian"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3382295684"));
                    startActivity(intent);
                }else  if(p.equals("papà"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3929104619"));
                    startActivity(intent);
                }else  if(p.equals("fabrizio"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3394695826"));
                    startActivity(intent);
                }else  if(p.equals("nonnacaterina"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3202780531"));
                    startActivity(intent);
                }else  if(p.equals("zioantonio"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3313749339"));
                    startActivity(intent);
                }else  if(p.equals("ilaria"))
                {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "3286055686"));
                    startActivity(intent);
                }

            }
            else if (s == false) {
                speak1("me rutt o cazz");
            }




        }catch(Exception e)

    {



    }

    switchSearch(KWS_SEARCH);
}


    private void initializeTextToSpeech() {
        myTTS= new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if(myTTS.getEngines().size()==0)
                {
                    finish();
                } else{
                    myTTS.setLanguage(Locale.ITALIAN);

                    speak1("Sto Ascoltando");

                }
            }
        });
    }

    private void speak1(String Message) {
        if(Build.VERSION.SDK_INT>=21)

        {

            myTTS.speak(Message, TextToSpeech.QUEUE_FLUSH, null, null);

        }else
        {
            myTTS.speak(Message,TextToSpeech.QUEUE_FLUSH,null);
        }
    }
private void parla()
{
    recognizer.stop();
    Intent in = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
    in.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
    in.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
    sp.startListening(in);


}


    private static class SetupTask extends AsyncTask<Void, Void, Exception> {
        WeakReference<PocketSphinxActivity> activityReference;
        SetupTask(PocketSphinxActivity activity) {
            this.activityReference = new WeakReference<>(activity);
        }
        @Override
        protected Exception doInBackground(Void... params) {
            try {
                Assets assets = new Assets(activityReference.get());
                File assetDir = assets.syncAssets();
                activityReference.get().setupRecognizer(assetDir);
            } catch (IOException e) {
                return e;
            }
            return null;
        }
        @Override
        protected void onPostExecute(Exception result) {
            if (result != null) {
                ((TextView) activityReference.get().findViewById(R.id.caption_text))
                        .setText("Failed to init recognizer " + result);
            } else {
                activityReference.get().switchSearch(KWS_SEARCH);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull  int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Recognizer initialization is a time-consuming and it involves IO,
                // so we execute it in async task
                new SetupTask(this).execute();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myTTS.stop();
        sp.stopListening();
        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }


    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();

        if (text.equals(KEYPHRASE))
        {
            recognizer.stop();
            myTTS.stop();
            Intent in = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            in.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            in.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
            sp.startListening(in);
        }
        else
            ((TextView) findViewById(R.id.result_text)).setText(text);
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        ((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
           // makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onBeginningOfSpeech() {

    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        String caption = getResources().getString(captions.get(searchName));
        ((TextView) findViewById(R.id.caption_text)).setText(caption);
    }

    private void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "it"))
                .setDictionary(new File(assetsDir, "it.dic"))

                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)

                .getRecognizer();
        recognizer.addListener(this);

        /* In your application you might not need to add all those searches.
          They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        File digitsGrammar = new File(assetsDir, "digits.gram");
        recognizer.addGrammarSearch(KWS_SEARCH ,digitsGrammar);
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);


        // Create grammar-based search for selection between demos

    }

    @Override
    public void onError(Exception error) {
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }
}
